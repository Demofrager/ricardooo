# Tutorial to run locally on a Docker in docker container #

Add a gitlab-runner with privileges to be able to run Docker in Docker

```shell
sudo gitlab-runner register -n --url GITLAB_URL --registration-token TOKEN --executor docker --description "Final example" --docker-image "docker:19.03.12" --docker-privileged --docker-volumes "/certs/client" --tag-list build,test
```

*GITLAB_URL* and *TOKEN* can be found in **SETTINGS>CI/CD>RUNNERS>SPECIFIC RUNNERS**

Create a .gitlab-ci.yml with:
```shell
image: docker:19.03.13

services:
  - docker:19.03.13-dind
```

If the gitlab runner was created with the base image of  docker:19.03.13, then that can be excluded from the .yml :metal:
