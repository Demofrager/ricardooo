FROM openjdk:11-jre-slim
EXPOSE 8080
ADD target/*.jar .
RUN mv *.jar app.jar
CMD exec java $JAVA_OPTS -jar app.jar
